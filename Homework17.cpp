// Homework17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class point
{
private:
    double x, y , z;
public:

    double GetX()
    {
        return x;
    }

    double GetY()
    {
        return y;
    }

    double GetZ()
    {
        return z;
    }

    void SetCoord(double xCoord, double yCoord, double zCoord)
    {
        x = xCoord;
        y = yCoord;
        z = zCoord;
    }

    void SetX(double xCoord)
    {
        x = xCoord;
    }

    void SetY(double yCoord)
    {
        y = yCoord;
    }

    void SetZ(double zCoord)
    {
        z = zCoord;
    }

};

class Vector
{
private:
    point Coord;

public:
    Vector()
    {
        Coord.SetCoord(0, 0, 0);
    }
    Vector(double xCoord, double yCoord, double zCoord)
    {
        Coord.SetCoord(xCoord, yCoord, zCoord);
    }
    void Show()
    {
        std::cout << '\n' << Coord.GetX() << ' ' << Coord.GetY() << ' ' << Coord.GetZ();
    }
    double Module() 
    {
        return sqrt(pow(Coord.GetX(),2) + pow(Coord.GetY(), 2) + pow(Coord.GetZ(), 2));
    }
    void SetCoord(double xCoord, double yCoord, double zCoord)
    {
        Coord.SetCoord(xCoord, yCoord, zCoord);
    }
};

int main()
{
    point P1;

    P1.SetCoord(1, 2, 3);

    std::cout << "Test SetCoord: \n";
    std::cout << "x: " << P1.GetX() << " y: " << P1.GetY() << " z: " << P1.GetZ() << '\n';

    P1.SetX(5);
    P1.SetY(8);
    P1.SetZ(10);

    std::cout << "Test SetX, SetY: \n";
    std::cout << "x: " << P1.GetX() << " y: " << P1.GetY() << " z: " << P1.GetZ() << '\n';

    Vector V1;
    V1.SetCoord(1, 2, 4);

    std::cout << "Test Vector: ";
    V1.Show();

    std::cout << '\n' << "Test Vector module: \n";
    std::cout << V1.Module();
}

